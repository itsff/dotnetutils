﻿/*
 * 
 *  Copyright (C) 2011 by Filip Frącz
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicallyMe.Utils;

namespace Tests
{
    [TestClass]
    public class LruFixedSizeCacheTests
    {
        public LruFixedSizeCacheTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get;
            set;
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void LRU_constructor_should_throw_on_zero_size()
        {
            new LruFixedSizeCache<int, string>(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void LRU_constructor_should_throw_on_negative_size()
        {
            new LruFixedSizeCache<int, string>(-1);
        }

        [TestMethod]
        public void LRU_first_should_be_last()
        {
            var lruCache = new LruFixedSizeCache<int, string>(1);

            lruCache.Add(0, "zero");
            Assert.AreEqual(lruCache.MostRecentlyUsed, lruCache.LeastRecentlyUsed);
        }

        [TestMethod]
        public void LRU_max_size_test()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            for (int i = 0; i < 10; ++i)
            {
                lruCache.Add(i, i.ToString());
            }

            Assert.AreEqual(3, lruCache.Count);
        }

        [TestMethod]
        public void LRU_eviction_test_1()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");
            lruCache.Add(3, "three");

            Assert.AreEqual("three", lruCache.MostRecentlyUsed);
            Assert.AreEqual("one", lruCache.LeastRecentlyUsed);
        }

        [TestMethod]
        public void LRU_eviction_test_2()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");
            lruCache.Add(3, "three");

            Assert.AreEqual(false, lruCache.ContainsKey(0));
        }

        private static void ensureRange<T>(IEnumerable<T> expectedItems, IEnumerable<T> testItems)
        {
            ensureRange(expectedItems, testItems.GetEnumerator());
        }

        private static void ensureRange<T>(IEnumerable<T> expectedItems, IEnumerator<T> testEnumerator)
        {
            IEnumerator<T> expectedEnumerator = expectedItems.GetEnumerator();

            while (expectedEnumerator.MoveNext() && testEnumerator.MoveNext())
            {
                Assert.AreEqual(expectedEnumerator.Current, testEnumerator.Current);
            }
        }

        [TestMethod]
        public void LRU_ordering_test_1()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");
            lruCache.Add(3, "three");

            ensureRange(new string[] { "three", "two", "one" }, lruCache);
        }

        [TestMethod]
        public void LRU_ordering_test_2()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");
            lruCache.Add(3, "three");

            string x = lruCache[2];

            ensureRange(new string[] { "two", "three", "one" }, lruCache);
        }

        [TestMethod]
        public void LRU_ordering_test_3()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");
            lruCache.Add(3, "three");
            
            string x = null;

            x = lruCache[2];
            x = lruCache[1];

            ensureRange(new string[] { "one", "two", "three" }, lruCache);
        }

        [TestMethod]
        public void LRU_ordering_test_4()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");

            Assert.Equals(3, lruCache.Count);
            ensureRange(new string[] { "zero", "one", "two" }, lruCache);
        }

        [TestMethod]
        public void LRU_clear_test_1()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");

            lruCache.Clear();
            Assert.Equals(0, lruCache.Count);
        }

        [TestMethod]
        public void LRU_clear_test_2()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);

            lruCache.Clear();
            Assert.Equals(0, lruCache.Count);
        }

        [TestMethod]
        public void LRU_count_test_1()
        {
            var lruCache = new LruFixedSizeCache<int, string>(3);
            Assert.Equals(0, lruCache.Count);
        }

        [TestMethod]
        public void LRU_count_test_2()
        {
            var lruCache = new LruFixedSizeCache<int, string>(1);

            lruCache.Add(0, "zero");
            lruCache.Add(1, "one");
            lruCache.Add(2, "two");

            Assert.Equals(1, lruCache.Count);
            Assert.Equals(1, lruCache.Capacity);
        }
    }

}

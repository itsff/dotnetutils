﻿/*
 * 
 *  Copyright (C) 2011 by Filip Frącz
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BasicallyMe.Utils
{
    public abstract class FixedSizeCache<TKey, TValue>
        : IDictionary<TKey, TValue>
        , IEnumerable<TValue>
    {
        protected readonly Dictionary<TKey, LinkedListNode<KeyValuePair<TKey,TValue>>> _map;
        protected readonly LinkedList<KeyValuePair<TKey, TValue>> _order;

        public FixedSizeCache(int capacity)
        {
            if (capacity <= 0)
            {
                throw new ArgumentOutOfRangeException("capacity", "Capacity must be greater than zero");
            }

            Capacity = capacity;
            _map    = new Dictionary<TKey, LinkedListNode<KeyValuePair<TKey, TValue>>>(Capacity);
            _order  = new LinkedList<KeyValuePair<TKey, TValue>>();
        }

        public FixedSizeCache(int capacity, IEnumerable<KeyValuePair<TKey, TValue>> initialItems)
            : this(capacity)
        {
            if (initialItems == null)
            {
                throw new ArgumentNullException("initialItems");
            }

            foreach (var pair in initialItems)
            {
                this[pair.Key] = pair.Value;
            }
        }

        public int Capacity { get; private set; }

        protected abstract void OnCacheSizeExceeded();

        private void moveToFront(LinkedListNode<KeyValuePair<TKey, TValue>> node)
        {
            _order.Remove(node);
            _order.AddFirst(node);
        }

        private void enforceCacheSize()
        {
            if (_map.Count > Capacity)
            {
                OnCacheSizeExceeded();
            }
        }

        public TValue MostRecentlyUsed
        {
            get
            {
                var node = _order.First;
                if (node != null)
                {
                    KeyValuePair<TKey, TValue> pair = node.Value;
                    return pair.Value;
                }
                return default(TValue);
            }
        }

        public TValue LeastRecentlyUsed
        {
            get
            {
                var node = _order.Last;
                if (node != null)
                {
                    KeyValuePair<TKey, TValue> pair = node.Value;
                    return pair.Value;
                }
                return default(TValue);
            }
        }
               

        #region IEnumerable<TValue> Members

        public IEnumerator<TValue> GetEnumerator()
        {
            return _order.Select(p => p.Value).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _order.GetEnumerator();
        }

        #endregion

        #region IDictionary<TKey,TValue> Members

        public void Add(TKey key, TValue value)
        {
            // New item being added
            var node = new LinkedListNode<KeyValuePair<TKey, TValue>>(new KeyValuePair<TKey,TValue>(key,value));
            _map.Add(key, node);
            _order.AddFirst(node);

            enforceCacheSize();            
        }

        public bool ContainsKey(TKey key)
        {
            return _map.ContainsKey(key);
        }

        public ICollection<TKey> Keys
        {
            get { return _map.Keys; }
        }

        public bool Remove(TKey key)
        {
            bool removed = false;

            LinkedListNode<KeyValuePair<TKey, TValue>> node = null;
            if (_map.TryGetValue(key, out node))
            {
                _map.Remove(key);
                _order.Remove(node);
                removed = true;
            }
            return removed;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            LinkedListNode<KeyValuePair<TKey, TValue>> node = null;
            if (_map.TryGetValue(key, out node))
            {
                value = node.Value.Value;
                moveToFront(node);
                return true;
            }
            else
            {
                value = default(TValue);
                return false;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                LinkedListNode<KeyValuePair<TKey, TValue>> node = _map[key];
                moveToFront(node);
                return node.Value.Value;
            }
            set
            {
                LinkedListNode<KeyValuePair<TKey, TValue>> node = null;
                
                if (_map.TryGetValue(key, out node))
                {
                    // Item already in our collection
                    node.Value = new KeyValuePair<TKey, TValue>(key, value);
                    moveToFront(node);
                }
                else
                {
                    Add(key, value);
                }                
            }
        }

        public void Clear()
        {
            _map.Clear();
            _order.Clear();
        }

        public int Count
        {
            get { return _map.Count; }
        }

        ICollection<TValue> IDictionary<TKey, TValue>.Values
        {
            get { return _order.Select(p => p.Value).ToList(); }
        }        

        #endregion


        #region ICollection<KeyValuePair<TKey,TValue>> Members

        void ICollection<KeyValuePair<TKey,TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        bool ICollection<KeyValuePair<TKey,TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        void ICollection<KeyValuePair<TKey,TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }      

        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
        {
            get { return false; }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            return _map.Keys
                .Select( key => new KeyValuePair<TKey, TValue>(key, _map[key].Value.Value))
                .GetEnumerator();
        }

        #endregion
    }


    public class LruFixedSizeCache<TKey, TValue> : FixedSizeCache<TKey, TValue>
    {
        public LruFixedSizeCache(int capacity)
            : base(capacity)
        {
        }

        public LruFixedSizeCache(int capacity, IEnumerable<KeyValuePair<TKey, TValue>> initialItems)
            : base(capacity, initialItems)
        {
        }

        protected override void OnCacheSizeExceeded()
        {
            // Evict least recently used item
            LinkedListNode<KeyValuePair<TKey,TValue>> node = _order.Last;

            _map.Remove(node.Value.Key);
            _order.RemoveLast();
        }
    }

    public class MruFixedSizeCache<TKey, TValue> : FixedSizeCache<TKey, TValue>
    {
        public MruFixedSizeCache(int capacity)
            : base(capacity)
        {
        }

        public MruFixedSizeCache(int capacity, IEnumerable<KeyValuePair<TKey, TValue>> initialItems)
            : base(capacity, initialItems)
        {           
        }         

        protected override void OnCacheSizeExceeded()
        {
            // Evict least recently used item
            LinkedListNode<KeyValuePair<TKey, TValue>> node = _order.First;

            _map.Remove(node.Value.Key);
            _order.RemoveFirst();
        }
    }
}
